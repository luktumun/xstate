import { useState, useEffect, useRef } from "react";
import React from "react";
import "./Xstate.css";
const Xstate = () => {
  const [country, setCountry] = useState("");
  const [state, setState] = useState("");
  const [city, setCity] = useState("");
  const [url, setUrl] = useState("");
  const [textNode, setTextNode] = useState("");

  const [data, setData] = useState([]);
  const [data1, setData1] = useState([]);
  const [data2, setData2] = useState([]);

  function handleCountryChange(event) {
    setCountry(event.target.value);
  }
  function handleStateChange(event) {
    setState(event.target.value);
  }
  function handleCityChange(event) {
    setCity(event.target.value);
  }
  function parr(event) {
    var z = document.createElement("p"); // is a node

    z.innerHTML = "You selected " + city + ", " + state + ", " + country + "";

    document.body.appendChild(z);
  }

  useEffect(() => {
    fetch("https://crio-location-selector.onrender.com/countries")
      .then((response) => response.json())
      .then((data) => setData(data))
      .catch((err) => {
        console.log(err);
      });
  }, []);
  useEffect(() => {
    fetch("https://crio-location-selector.onrender.com/country=India/states")
      .then((response) => response.json())
      .then((data1) => setData1(data1))
      .catch((err) => {
        console.log(err);
      });
  }, []);
  useEffect(() => {
    fetch(
      "https://crio-location-selector.onrender.com/country=India/state=Goa/Cities"
    )
      .then((response) => response.json())
      .then((data2) => setData2(data2))
      .catch((err) => {
        console.log(err);
      });
  }, []);

  console.log(country, state, city);

  return (
    <>
      <h1 className="header">Select Location</h1>
      <div className="div" id>
        <select value={country} onChange={handleCountryChange}>
          <option value="">Select Country</option>
          {data &&
            data.map((item) => (
              <option key={item} value={item}>
                {item}
              </option>
            ))}
        </select>
        <select value={state} onChange={handleStateChange}>
          <option value="">Select State</option>
          {data1 &&
            data1.map((item) => (
              <option value={item} key={item}>
                {item}
              </option>
            ))}
        </select>
        <select value={city} onChange={handleCityChange}>
          <option value="">Select City</option>

          {data2 &&
            data2.map((item) => (
              <option key={item} value={item}>
                {item}
              </option>
            ))}
        </select>
      </div>
      <p className="msg_txt">
        You selected {city}, {state}, {country}
      </p>
    </>
  );
};

export default Xstate;
